﻿using DataAccess.Entities;

namespace DataAccess
{
    public class OngoingCourseLinesRepository : BaseRepository<OngoingCourseLine>
    {
        public OngoingCourseLinesRepository(CoursesDbContext context) : base(context)
        {
        }
    }
}
