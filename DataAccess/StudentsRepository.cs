﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class StudentsRepository : BaseRepository<Student>
    {
        public StudentsRepository(CoursesDbContext context) : base(context)
        {
        }


    }
}
