﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Entities
{
    public class Course : BaseEntity
    {
        [Column(TypeName = "decimal(7, 2)")]
        public decimal Price { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        public int Hours { get; set; }

        [Required]
        [StringLength(250)]
        public string Description { get; set; }

        public override string ToString()
        {
            return $"{Id}/{Price}/{Title}/{Hours}/{Description}";
        }
    }
}
