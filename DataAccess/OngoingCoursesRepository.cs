﻿using DataAccess.Entities;

namespace DataAccess
{
    public class OngoingCoursesRepository : BaseRepository<OngoingCourse>
    {
        public OngoingCoursesRepository(CoursesDbContext context) : base(context)
        {
        }
    }
}
