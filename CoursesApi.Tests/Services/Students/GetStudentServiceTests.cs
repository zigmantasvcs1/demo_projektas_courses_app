﻿using CoursesApi.Models.Students.Parameters;
using DataAccess.Entities;
using DataAccess;
using Moq;
using CoursesApi.Services.Students;

namespace CoursesApi.Tests.Services.Students
{
    [TestClass]
    public class GetStudentServiceTests
    {
        private GetStudentParameter parameter = null!;
        private Student studentRepositoryResult = null!;

        private Mock<IRepository<Student>> studentRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateGetStudentParameter();

            studentRepositoryResult = CreateStudent();

            studentRepositoryMock = GetStudentRepositoryMock();
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await service.CallAsync(null));
        }

        [TestMethod]
        public async Task CallsStudentRepositoryOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            studentRepositoryMock.Verify(s => s.GetAsync(parameter.Id), Times.Once);
            studentRepositoryMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task ResultOfServiceContainsValuesFromStudentRepository()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(studentRepositoryResult.Id, result.Data.Id);
            Assert.AreEqual(studentRepositoryResult.Name, result.Data.Name);
            Assert.AreEqual(studentRepositoryResult.Surname, result.Data.Surname);
            Assert.AreEqual(studentRepositoryResult.Email, result.Data.Email);
            Assert.AreEqual(studentRepositoryResult.DocumentNumber, result.Data.DocumentNumber);
            Assert.AreEqual(studentRepositoryResult.BirthDay, result.Data.BirthDay);
        }

        private GetStudentService CreateService()
        {
            return new GetStudentService(studentRepositoryMock.Object);
        }

        private GetStudentParameter CreateGetStudentParameter()
        {
            return new GetStudentParameter(1);
        }

        private Student CreateStudent()
        {
            return new Student()
            {
                Id = 111,
                Name = "test",
                Surname = "testinukas",
                Email = "testinukas@gmail.com",
                DocumentNumber = "AAA001",
                BirthDay = new DateTime(1999, 1, 1),
                CreatedAt = new DateTime(2023, 12, 10)
            };
        }

        private Mock<IRepository<Student>> GetStudentRepositoryMock()
        {
            var mock = new Mock<IRepository<Student>>();

            mock.Setup(repo => repo.GetAsync(It.IsAny<int>())).ReturnsAsync(studentRepositoryResult);

            return mock;
        }
    }
}
