﻿using CoursesApi.Models.Students.Dtos;
using CoursesApi.Models.Students.Parameters;
using CoursesApi.Models;
using CoursesApi.Services;
using Microsoft.Extensions.Logging;
using Moq;
using CoursesApi.Services.Students;

namespace CoursesApi.Tests.Services.Students
{
    [TestClass]
    public class GetStudentServiceDecoratorTests
    {
        private GetStudentParameter parameter = null!;
        private Mock<IService<GetStudentParameter, StudentDto>> getStudentServiceMock = null!;
        private Mock<ILogger<GetStudentServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateGetStudentParameter();

            getStudentServiceMock = new Mock<IService<GetStudentParameter, StudentDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2)
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            getStudentServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<StudentDto>(200, new StudentDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            getStudentServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            getStudentServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private GetStudentServiceDecorator CreateService()
        {
            return new GetStudentServiceDecorator(
                getStudentServiceMock.Object,
                loggerMock.Object
            );
        }

        private GetStudentParameter CreateGetStudentParameter()
        {
            return new GetStudentParameter(1);
        }

        private Mock<ILogger<GetStudentServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<GetStudentServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
