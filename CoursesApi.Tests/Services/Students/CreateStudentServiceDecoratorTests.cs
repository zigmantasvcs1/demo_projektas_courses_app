﻿using CoursesApi.Models.Students.Dtos;
using CoursesApi.Models.Students.Parameters;
using CoursesApi.Models;
using CoursesApi.Services;
using Microsoft.Extensions.Logging;
using Moq;
using CoursesApi.Services.Students;

namespace CoursesApi.Tests.Services.Students
{
    [TestClass]
    public class CreateStudentServiceDecoratorTests
    {
        private CreateStudentParameter parameter = null!;
        private Mock<IService<CreateStudentParameter, StudentDto>> createStudentServiceMock = null!;
        private Mock<ILogger<CreateStudentServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateCreateStudentParameter();

            createStudentServiceMock = new Mock<IService<CreateStudentParameter, StudentDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2) // Expect at least two log entries - one at the start and one at the end
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            createStudentServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<StudentDto>(200, new StudentDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            createStudentServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            createStudentServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private CreateStudentServiceDecorator CreateService()
        {
            return new CreateStudentServiceDecorator(
                createStudentServiceMock.Object,
                loggerMock.Object
            );
        }

        private CreateStudentParameter CreateCreateStudentParameter()
        {
            return new CreateStudentParameter(new CreateStudentDto());
        }

        private Mock<ILogger<CreateStudentServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<CreateStudentServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
