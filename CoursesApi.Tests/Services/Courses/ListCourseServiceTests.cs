﻿using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Services.Courses;
using DataAccess;
using DataAccess.Entities;
using Moq;
using System.Linq.Expressions;

namespace CoursesApi.Tests.Services.Courses
{
    [TestClass]
    public class ListCourseServiceTests
    {
        private ListCourseParameter parameter = null!;
        private List<Course> lecturerRepositoryResult = null!;
        private Mock<IRepository<Course>> lecturersRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = new ListCourseParameter(5);
            lecturerRepositoryResult = CreateCoursesList();
            lecturersRepositoryMock = new Mock<IRepository<Course>>();
            lecturersRepositoryMock
                .Setup(repo => repo.ListAsync(It.IsAny<int>(), It.IsAny<Expression<Func<Course, bool>>>()))
                .ReturnsAsync(lecturerRepositoryResult);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsListOfCourses()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(lecturerRepositoryResult.Count, result.Data.Count);
            for (int i = 0; i < lecturerRepositoryResult.Count; i++)
            {
                Assert.AreEqual(lecturerRepositoryResult[i].Id, result.Data[i].Id);
                Assert.AreEqual(lecturerRepositoryResult[i].Title, result.Data[i].Title);
                Assert.AreEqual(lecturerRepositoryResult[i].Description, result.Data[i].Description);
                Assert.AreEqual(lecturerRepositoryResult[i].Hours, result.Data[i].Hours);
                Assert.AreEqual(lecturerRepositoryResult[i].Price, result.Data[i].Price);
            }
        }

        [TestMethod]
        public async Task CallsCoursesRepositoryListOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            lecturersRepositoryMock.Verify(
                s => s.ListAsync(parameter.Limit, null),
                Times.Once
            );
        }

        private ListCourseService CreateService()
        {
            return new ListCourseService(lecturersRepositoryMock.Object);
        }

        private List<Course> CreateCoursesList()
        {
            return new List<Course>
            {
                new(),
            };
        }
    }
}
