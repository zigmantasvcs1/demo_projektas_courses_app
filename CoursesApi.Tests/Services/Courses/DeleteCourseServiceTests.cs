﻿using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Services.Courses;
using DataAccess;
using DataAccess.Entities;
using Moq;

namespace CoursesApi.Tests.Services.Courses
{
    [TestClass]
    public class DeleteCourseServiceTests
    {
        private DeleteCourseParameter parameter = null!;
        private Mock<IRepository<Course>> lecturerRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = new DeleteCourseParameter(1);
            lecturerRepositoryMock = new Mock<IRepository<Course>>();
        }

        private DeleteCourseService CreateService()
        {
            return new DeleteCourseService(lecturerRepositoryMock.Object);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsSuccessResultWhenDeleteIsSuccessful()
        {
            // arrange
            lecturerRepositoryMock.Setup(repo => repo.DeleteAsync(It.IsAny<int>())).ReturnsAsync(true);
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
        }

        [TestMethod]
        public async Task ReturnsNotFoundResultWhenDeleteFails()
        {
            // arrange
            lecturerRepositoryMock
                .Setup(repo => repo.DeleteAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(404, result.Status);
            Assert.IsNull(result.Data);
        }

        [TestMethod]
        public async Task CallsCourseRepositoryDeleteOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            lecturerRepositoryMock.Verify(s => s.DeleteAsync(parameter.Id), Times.Once);
        }
    }
}
