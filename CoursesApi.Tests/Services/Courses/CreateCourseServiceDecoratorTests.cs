﻿using CoursesApi.Models.Courses.Dtos;
using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Models;
using CoursesApi.Services.Courses;
using Microsoft.Extensions.Logging;
using Moq;
using CoursesApi.Services;

namespace CoursesApi.Tests.Services.Courses
{
    [TestClass]
    public class CreateCourseServiceDecoratorTests
    {
        private CreateCourseParameter parameter = null!;
        private Mock<IService<CreateCourseParameter, CourseDto>> createCourseServiceMock = null!;
        private Mock<ILogger<CreateCourseServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateCreateCourseParameter();

            createCourseServiceMock = new Mock<IService<CreateCourseParameter, CourseDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2) // Expect at least two log entries - one at the start and one at the end
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            createCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<CourseDto>(200, new CourseDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            createCourseServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            createCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private CreateCourseServiceDecorator CreateService()
        {
            return new CreateCourseServiceDecorator(
                createCourseServiceMock.Object,
                loggerMock.Object
            );
        }

        private CreateCourseParameter CreateCreateCourseParameter()
        {
            return new CreateCourseParameter(new CreateCourseDto());
        }

        private Mock<ILogger<CreateCourseServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<CreateCourseServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
