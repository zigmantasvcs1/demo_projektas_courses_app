﻿using CoursesApi.Models.Courses.Dtos;
using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Models.Lecturers.Dtos;
using CoursesApi.Services.Courses;
using DataAccess;
using DataAccess.Entities;
using Moq;

namespace CoursesApi.Tests.Services.Courses
{
    [TestClass]
    public class UpdateCourseServiceTests
    {
        private UpdateCourseParameter parameter = null!;
        private Course lecturerRepositoryUpdateResult = null!;
        private Course lecturerRepositoryGetResult = null!;
        private Mock<IRepository<Course>> lecturerRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            var updateCourseDto = new UpdateCourseDto
            {
                // Initialize with test data
            };
            parameter = new UpdateCourseParameter(updateCourseDto);
            lecturerRepositoryUpdateResult = CreateCourse();
            lecturerRepositoryGetResult = CreateCourse();
            lecturerRepositoryMock = new Mock<IRepository<Course>>();

            lecturerRepositoryMock
                .Setup(lecturerRepository => lecturerRepository.GetAsync(It.IsAny<int>()))
                .ReturnsAsync(lecturerRepositoryGetResult);

            lecturerRepositoryMock
                .Setup(repo => repo.UpdateAsync(It.IsAny<Course>()))
                .ReturnsAsync(lecturerRepositoryGetResult);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsUpdatedCourseDto()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(lecturerRepositoryUpdateResult.Id, result.Data.Id);
            Assert.AreEqual(lecturerRepositoryUpdateResult.Title, result.Data.Title);
        }

        [TestMethod]
        public async Task CallsCourseRepositoryUpdateOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            lecturerRepositoryMock.Verify(s => s.UpdateAsync(It.IsAny<Course>()), Times.Once);
        }

        private UpdateCourseService CreateService()
        {
            return new UpdateCourseService(lecturerRepositoryMock.Object);
        }

        private Course CreateCourse()
        {
            return new Course();
        }
    }
}
