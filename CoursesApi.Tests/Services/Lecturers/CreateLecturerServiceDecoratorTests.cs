﻿using CoursesApi.Models.Lecturers.Dtos;
using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Models;
using CoursesApi.Services.Lecturers;
using Microsoft.Extensions.Logging;
using Moq;
using CoursesApi.Services;

namespace LecturersApi.Tests.Services.Lecturers
{
    [TestClass]
    public class CreateLecturerServiceDecoratorTests
    {
        private CreateLecturerParameter parameter = null!;
        private Mock<IService<CreateLecturerParameter, LecturerDto>> createLecturerServiceMock = null!;
        private Mock<ILogger<CreateLecturerServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateCreateLecturerParameter();

            createLecturerServiceMock = new Mock<IService<CreateLecturerParameter, LecturerDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2) // Expect at least two log entries - one at the start and one at the end
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            createLecturerServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<LecturerDto>(200, new LecturerDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            createLecturerServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            createLecturerServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private CreateLecturerServiceDecorator CreateService()
        {
            return new CreateLecturerServiceDecorator(
                createLecturerServiceMock.Object,
                loggerMock.Object
            );
        }

        private CreateLecturerParameter CreateCreateLecturerParameter()
        {
            return new CreateLecturerParameter(new CreateLecturerDto());
        }

        private Mock<ILogger<CreateLecturerServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<CreateLecturerServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
