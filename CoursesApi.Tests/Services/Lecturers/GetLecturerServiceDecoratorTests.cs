﻿using CoursesApi.Models.Lecturers.Dtos;
using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Models;
using CoursesApi.Services.Lecturers;
using CoursesApi.Services;
using Microsoft.Extensions.Logging;
using Moq;

namespace LecturersApi.Tests.Services.Lecturers
{
    [TestClass]
    public class GetLecturerServiceDecoratorTests
    {
        private GetLecturerParameter parameter = null!;
        private Mock<IService<GetLecturerParameter, LecturerDto>> getLecturerServiceMock = null!;
        private Mock<ILogger<GetLecturerServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateGetLecturerParameter();

            getLecturerServiceMock = new Mock<IService<GetLecturerParameter, LecturerDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2)
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            getLecturerServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<LecturerDto>(200, new LecturerDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            getLecturerServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            getLecturerServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private GetLecturerServiceDecorator CreateService()
        {
            return new GetLecturerServiceDecorator(
                getLecturerServiceMock.Object,
                loggerMock.Object
            );
        }

        private GetLecturerParameter CreateGetLecturerParameter()
        {
            return new GetLecturerParameter(1);
        }

        private Mock<ILogger<GetLecturerServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<GetLecturerServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
