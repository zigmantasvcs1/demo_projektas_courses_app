﻿using CoursesApi.Models.Lecturers.Dtos;
using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Providers;
using CoursesApi.Services.Lecturers;
using DataAccess;
using DataAccess.Entities;
using Moq;

namespace CoursesApi.Tests.Services.Lecturers
{
    [TestClass]
    public class CreateLecturerServiceTests
    {
        private CreateLecturerParameter parameter = null!;

        private Lecturer lecturerRepositoryResult = null!;
        private DateTime dateTimeNowProviderResult;

        private Mock<IRepository<Lecturer>> lecturerRepositoryMock = null!;
        private Mock<IDateTimeNowProvider> dateTimeNowProviderMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateCreateLecturerParameter();

            dateTimeNowProviderResult = new DateTime(1999, 1, 1);

            lecturerRepositoryResult = CreateLecturer();

            lecturerRepositoryMock = GetLecturerRepositoryMock();
            dateTimeNowProviderMock = GetDateTimeNowProviderMock();
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await service.CallAsync(null));
        }

        [TestMethod]
        public async Task CallsLecturerRepositoryOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            lecturerRepositoryMock.Verify(
                s => s.CreateAsync(
                    It.Is<Lecturer>(
                        fromMock => fromMock.Id == 0
                        && fromMock.Name == "test"
                        && fromMock.Surname == "testinukas"
                        && fromMock.Email == "testinukas@gmail.com"
                        && fromMock.DocumentNumber == "AAA001"
                        && fromMock.CreatedAt == dateTimeNowProviderResult
                    )
                )
            );

            lecturerRepositoryMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task ResultOfServiceContainsValuesFromLecturerRepository()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            CollectionAssert.AreEqual(new List<string>(), result.Errors);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(111, result.Data.Id);
            Assert.AreEqual("test", result.Data.Name);
            Assert.AreEqual("testinukas", result.Data.Surname);
            Assert.AreEqual("testinukas@gmail.com", result.Data.Email);
            Assert.AreEqual("AAA001", result.Data.DocumentNumber);
        }

        private CreateLecturerService CreateService()
        {
            return new CreateLecturerService(
                lecturerRepositoryMock.Object,
                dateTimeNowProviderMock.Object
            );
        }

        private CreateLecturerParameter CreateCreateLecturerParameter()
        {
            return new CreateLecturerParameter(
                new CreateLecturerDto()
                {
                    Name = "test",
                    Surname = "testinukas",
                    Email = "testinukas@gmail.com",
                    DocumentNumber = "AAA001"
                }
            );
        }

        private Lecturer CreateLecturer()
        {
            return new Lecturer()
            {
                Id = 111,
                Name = "test",
                Surname = "testinukas",
                Email = "testinukas@gmail.com",
                DocumentNumber = "AAA001",
                CreatedAt = new DateTime(2023, 12, 10)
            };
        }

        private Mock<IRepository<Lecturer>> GetLecturerRepositoryMock()
        {
            var mock = new Mock<IRepository<Lecturer>>();

            mock
                .Setup(
                    repo => repo.CreateAsync(It.IsAny<Lecturer>())
                )
                .ReturnsAsync(lecturerRepositoryResult);

            return mock;
        }

        private Mock<IDateTimeNowProvider> GetDateTimeNowProviderMock()
        {
            var mock = new Mock<IDateTimeNowProvider>();

            mock
                .Setup(
                    provider => provider.Get()
                )
                .Returns(dateTimeNowProviderResult);

            return mock;
        }
    }
}
