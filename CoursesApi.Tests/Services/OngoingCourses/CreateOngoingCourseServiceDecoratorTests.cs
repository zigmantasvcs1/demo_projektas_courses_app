﻿using CoursesApi.Models.OngoingCourses.Dtos;
using CoursesApi.Models.OngoingCourses.Parameters;
using CoursesApi.Models;
using CoursesApi.Services;
using Microsoft.Extensions.Logging;
using Moq;
using CoursesApi.Services.OngoingCourses;

namespace CoursesApi.Tests.Services.OngoingCourses
{
    [TestClass]
    public class CreateOngoingCourseServiceDecoratorTests
    {
        private CreateOngoingCourseParameter parameter = null!;
        private Mock<IService<CreateOngoingCourseParameter, OngoingCourseDto>> createOngoingCourseServiceMock = null!;
        private Mock<ILogger<CreateOngoingCourseServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateCreateOngoingCourseParameter();

            createOngoingCourseServiceMock = new Mock<IService<CreateOngoingCourseParameter, OngoingCourseDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2) // Expect at least two log entries - one at the start and one at the end
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            createOngoingCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<OngoingCourseDto>(200, new OngoingCourseDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            createOngoingCourseServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            createOngoingCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private CreateOngoingCourseServiceDecorator CreateService()
        {
            return new CreateOngoingCourseServiceDecorator(
                createOngoingCourseServiceMock.Object,
                loggerMock.Object
            );
        }

        private CreateOngoingCourseParameter CreateCreateOngoingCourseParameter()
        {
            return new CreateOngoingCourseParameter(new CreateOngoingCourseDto());
        }

        private Mock<ILogger<CreateOngoingCourseServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<CreateOngoingCourseServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
