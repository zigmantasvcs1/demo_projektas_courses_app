﻿using CoursesApi.Models.OngoingCourses.Parameters;
using CoursesApi.Services.OngoingCourses;
using DataAccess;
using DataAccess.Entities;
using Moq;
using System.Linq.Expressions;

namespace CoursesApi.Tests.Services.OngoingCourses
{
    [TestClass]
    public class ListOngoingCourseServiceTests
    {
        private ListOngoingCourseParameter parameter = null!;
        private List<OngoingCourse> ongoingCourseRepositoryResult = null!;
        private Mock<IRepository<OngoingCourse>> ongoingCoursesRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = new ListOngoingCourseParameter(5);
            ongoingCourseRepositoryResult = CreateOngoingCoursesList();
            ongoingCoursesRepositoryMock = new Mock<IRepository<OngoingCourse>>();
            ongoingCoursesRepositoryMock
                .Setup(repo => repo.ListAsync(It.IsAny<int>(), It.IsAny<Expression<Func<OngoingCourse, bool>>>()))
                .ReturnsAsync(ongoingCourseRepositoryResult);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsListOfOngoingCourses()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(ongoingCourseRepositoryResult.Count, result.Data.Count);
            for (int i = 0; i < ongoingCourseRepositoryResult.Count; i++)
            {
                Assert.AreEqual(ongoingCourseRepositoryResult[i].Id, result.Data[i].Id);
                Assert.AreEqual(ongoingCourseRepositoryResult[i].StartDate, result.Data[i].StartDate);
                Assert.AreEqual(ongoingCourseRepositoryResult[i].EndDate, result.Data[i].EndDate);
                Assert.AreEqual(ongoingCourseRepositoryResult[i].Course.Id, result.Data[i].Course.Id);
                Assert.AreEqual(ongoingCourseRepositoryResult[i].Course.Title, result.Data[i].Course.Title);
                Assert.AreEqual(ongoingCourseRepositoryResult[i].Course.Description, result.Data[i].Course.Description);
                Assert.AreEqual(ongoingCourseRepositoryResult[i].Course.Hours, result.Data[i].Course.Hours);
                Assert.AreEqual(ongoingCourseRepositoryResult[i].Course.Price, result.Data[i].Course.Price);
            }
        }

        [TestMethod]
        public async Task CallsOngoingCoursesRepositoryListOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            ongoingCoursesRepositoryMock.Verify(
                s => s.ListAsync(parameter.Limit, null),
                Times.Once
            );
        }

        private ListOngoingCourseService CreateService()
        {
            return new ListOngoingCourseService(ongoingCoursesRepositoryMock.Object);
        }

        private List<OngoingCourse> CreateOngoingCoursesList()
        {
            return new List<OngoingCourse>
            {
                new()
                {
                    Id = 111,
                    CourseId = 1,
                    StartDate = new DateTime(1999, 1, 1),
                    EndDate = new DateTime(1999, 1, 31),
                    CreatedAt = new DateTime(2023, 12, 10),
                    Course = new Course()
                    {
                        Id = 1,
                        Title = "test",
                        Description = "testinukas",
                        Hours = 10,
                        Price = 20,
                        CreatedAt = new DateTime(2023, 12, 10)
                    }
                },
            };
        }
    }
}
