﻿using CoursesApi.Models.OngoingCourses.Dtos;
using CoursesApi.Models.OngoingCourses.Parameters;
using CoursesApi.Models;
using CoursesApi.Services;
using Microsoft.Extensions.Logging;
using Moq;
using CoursesApi.Services.OngoingCourses;

namespace CoursesApi.Tests.Services.OngoingCourses
{
    [TestClass]
    public class DeleteOngoingCourseServiceDecoratorTests
    {
        private DeleteOngoingCourseParameter parameter = null!;
        private Mock<IService<DeleteOngoingCourseParameter, OngoingCourseDto>> deleteOngoingCourseServiceMock = null!;
        private Mock<ILogger<DeleteOngoingCourseServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = new DeleteOngoingCourseParameter(1); // Assuming this is how you create your parameter
            deleteOngoingCourseServiceMock = new Mock<IService<DeleteOngoingCourseParameter, OngoingCourseDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2)
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            deleteOngoingCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<OngoingCourseDto>(200, new OngoingCourseDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            deleteOngoingCourseServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            deleteOngoingCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private DeleteOngoingCourseServiceDecorator CreateService()
        {
            return new DeleteOngoingCourseServiceDecorator(
                deleteOngoingCourseServiceMock.Object,
                loggerMock.Object
            );
        }

        private Mock<ILogger<DeleteOngoingCourseServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<DeleteOngoingCourseServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
