﻿using CoursesApi.Models.OngoingCourses.Dtos;
using CoursesApi.Models.OngoingCourses.Parameters;
using DataAccess.Entities;
using DataAccess;
using Moq;
using CoursesApi.Providers;
using CoursesApi.Services.OngoingCourses;

namespace CoursesApi.Tests.Services.OngoingCourses
{
    [TestClass]
    public class CreateOngoingCourseServiceTests
    {
        private CreateOngoingCourseParameter parameter = null!;

        private OngoingCourse ongoingCourseRepositoryResult = null!;
        private DateTime dateTimeNowProviderResult;

        private Mock<IRepository<OngoingCourse>> ongoingCourseRepositoryMock = null!;
        private Mock<IDateTimeNowProvider> dateTimeNowProviderMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateCreateOngoingCourseParameter();

            dateTimeNowProviderResult = new DateTime(1999, 1, 1);

            ongoingCourseRepositoryResult = CreateOngoingCourse();

            ongoingCourseRepositoryMock = GetOngoingCourseRepositoryMock();
            dateTimeNowProviderMock = GetDateTimeNowProviderMock();
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await service.CallAsync(null));
        }

        [TestMethod]
        public async Task CallsOngoingCourseRepositoryOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            ongoingCourseRepositoryMock.Verify(
                s => s.CreateAsync(
                    It.Is<OngoingCourse>(
                        fromMock => fromMock.Id == 0
                        && fromMock.CourseId == 1
                        && fromMock.StartDate == new DateTime(1999, 1, 1)
                        && fromMock.EndDate == new DateTime(1999, 1, 31)
                        && fromMock.CreatedAt == dateTimeNowProviderResult
                    )
                )
            );

            ongoingCourseRepositoryMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task ResultOfServiceContainsValuesFromOngoingCourseRepository()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            CollectionAssert.AreEqual(new List<string>(), result.Errors);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(111, result.Data.Id);
            Assert.AreEqual(new DateTime(1999, 1, 1), result.Data.StartDate);
            Assert.AreEqual(new DateTime(1999, 1, 31), result.Data.EndDate);
            Assert.AreEqual(null, result.Data.Course);

        }

        private CreateOngoingCourseService CreateService()
        {
            return new CreateOngoingCourseService(
                ongoingCourseRepositoryMock.Object,
                dateTimeNowProviderMock.Object
            );
        }

        private CreateOngoingCourseParameter CreateCreateOngoingCourseParameter()
        {
            return new CreateOngoingCourseParameter(
                new CreateOngoingCourseDto()
                {
                    CourseId = 1,
                    StartDate = new DateTime(1999, 1, 1),
                    EndDate = new DateTime(1999, 1, 31),
                }
            );
        }

        private OngoingCourse CreateOngoingCourse()
        {
            return new OngoingCourse()
            {
                Id = 111,
                CourseId = 1,
                StartDate = new DateTime(1999, 1, 1),
                EndDate = new DateTime(1999, 1, 31),
                CreatedAt = new DateTime(2023, 12, 10),
                Course = new Course()
                {
                    Id = 1,
                    Title = "test",
                    Description = "testinukas",
                    Hours = 10,
                    Price = 20,
                    CreatedAt = new DateTime(2023, 12, 10)
                }
            };
        }

        private Mock<IRepository<OngoingCourse>> GetOngoingCourseRepositoryMock()
        {
            var mock = new Mock<IRepository<OngoingCourse>>();

            mock
                .Setup(
                    repo => repo.CreateAsync(It.IsAny<OngoingCourse>())
                )
                .ReturnsAsync(ongoingCourseRepositoryResult);

            return mock;
        }

        private Mock<IDateTimeNowProvider> GetDateTimeNowProviderMock()
        {
            var mock = new Mock<IDateTimeNowProvider>();

            mock
                .Setup(
                    provider => provider.Get()
                )
                .Returns(dateTimeNowProviderResult);

            return mock;
        }
    }
}
