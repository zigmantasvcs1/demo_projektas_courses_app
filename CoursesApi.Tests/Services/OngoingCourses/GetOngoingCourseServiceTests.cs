﻿using CoursesApi.Models.OngoingCourses.Parameters;
using DataAccess.Entities;
using DataAccess;
using Moq;
using CoursesApi.Services.OngoingCourses;

namespace CoursesApi.Tests.Services.OngoingCourses
{
    [TestClass]
    public class GetOngoingCourseServiceTests
    {
        private GetOngoingCourseParameter parameter = null!;
        private OngoingCourse ongoingCourseRepositoryResult = null!;

        private Mock<IRepository<OngoingCourse>> ongoingCourseRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateGetOngoingCourseParameter();

            ongoingCourseRepositoryResult = CreateOngoingCourse();

            ongoingCourseRepositoryMock = GetOngoingCourseRepositoryMock();
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await service.CallAsync(null));
        }

        [TestMethod]
        public async Task CallsOngoingCourseRepositoryOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            ongoingCourseRepositoryMock.Verify(s => s.GetAsync(parameter.Id), Times.Once);
            ongoingCourseRepositoryMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task ResultOfServiceContainsValuesFromOngoingCourseRepository()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(ongoingCourseRepositoryResult.Id, result.Data.Id);
            Assert.AreEqual(ongoingCourseRepositoryResult.StartDate, result.Data.StartDate);
            Assert.AreEqual(ongoingCourseRepositoryResult.EndDate, result.Data.EndDate);
            Assert.AreEqual(ongoingCourseRepositoryResult.Course.Id, result.Data.Course.Id);
            Assert.AreEqual(ongoingCourseRepositoryResult.Course.Title, result.Data.Course.Title);
            Assert.AreEqual(ongoingCourseRepositoryResult.Course.Description, result.Data.Course.Description);
            Assert.AreEqual(ongoingCourseRepositoryResult.Course.Hours, result.Data.Course.Hours);
            Assert.AreEqual(ongoingCourseRepositoryResult.Course.Price, result.Data.Course.Price);
        }

        private GetOngoingCourseService CreateService()
        {
            return new GetOngoingCourseService(ongoingCourseRepositoryMock.Object);
        }

        private GetOngoingCourseParameter CreateGetOngoingCourseParameter()
        {
            return new GetOngoingCourseParameter(1);
        }

        private OngoingCourse CreateOngoingCourse()
        {
            return new OngoingCourse()
            {
                Id = 111,
                CourseId = 1,
                StartDate = new DateTime(1999, 1, 1),
                EndDate = new DateTime(1999, 1, 31),
                CreatedAt = new DateTime(2023, 12, 10),
                Course = new Course()
                {
                    Id = 1,
                    Title = "test",
                    Description = "testinukas",
                    Hours = 10,
                    Price = 20,
                    CreatedAt = new DateTime(2023, 12, 10)
                }
            };
        }

        private Mock<IRepository<OngoingCourse>> GetOngoingCourseRepositoryMock()
        {
            var mock = new Mock<IRepository<OngoingCourse>>();

            mock.Setup(repo => repo.GetAsync(It.IsAny<int>())).ReturnsAsync(ongoingCourseRepositoryResult);

            return mock;
        }
    }
}
