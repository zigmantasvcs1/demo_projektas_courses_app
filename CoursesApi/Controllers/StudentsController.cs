﻿using CoursesApi.Models;
using CoursesApi.Models.Students.Dtos;
using CoursesApi.Models.Students.Parameters;
using CoursesApi.Services;
using DataAccess.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CoursesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IService<ListStudentParameter, List<StudentDto>> _listStudentsService;
        private readonly IService<CreateStudentParameter, StudentDto> _createStudentsService;
        private readonly IService<UpdateStudentParameter, StudentDto> _updateStudentsService;
        private readonly IService<GetStudentParameter, StudentDto> _getStudentsService;
        private readonly IService<DeleteStudentParameter, StudentDto> _deleteStudentsService;

        public StudentsController(
            IService<ListStudentParameter, List<StudentDto>> listStudentsService,
            IService<CreateStudentParameter, StudentDto> createStudentsService,
            IService<UpdateStudentParameter, StudentDto> updateStudentsService,
            IService<GetStudentParameter, StudentDto> getStudentsService,
            IService<DeleteStudentParameter, StudentDto> deleteStudentsService)
        {
            _listStudentsService = listStudentsService;
            _createStudentsService = createStudentsService;
            _updateStudentsService = updateStudentsService;
            _getStudentsService = getStudentsService;
            _deleteStudentsService = deleteStudentsService;
        }

        [HttpGet]
        public async Task<IActionResult> ListAsync()
        {
            try
            {
                var students = await _listStudentsService.CallAsync(
                    new ListStudentParameter(10)
                );

                return Ok(students);
            }
            catch (Exception ex)
            {
                return StatusCode(
                    500, 
                    new Result<Student>(
                        500, 
                        null, 
                        new List<string>() { "Klaida" }
                    )
                );
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            try
            {
                var result =  await _getStudentsService.CallAsync(new GetStudentParameter(id));

                if (result.Data == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<Student>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreateStudentDto student)
        {
            var result = await _createStudentsService.CallAsync(
                new CreateStudentParameter(student)
            );

            if(result.Status == 200)
            {
                return Ok(result);
            }

            return StatusCode(
                result.Status,
                result
            );
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, UpdateStudentDto student)
        {
            try
            {
                if (id != student.Id)
                {
                    return BadRequest();
                }

                var result = await _updateStudentsService.CallAsync(
                    new UpdateStudentParameter(student)
                );

                return Ok(result);
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<Student>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                var result = await _deleteStudentsService.CallAsync(new DeleteStudentParameter(id));

                if (result.Data == null)
                {
                    return NotFound();
                }

                return NoContent();
            }
            catch (DbUpdateException)
            {
                return StatusCode(
                    422,
                    new Result<Student>(
                        422,
                        null,
                        new List<string>() { "Studentas yra naudojamas kitur." }
                    )
                );
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<Student>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }
    }
}
