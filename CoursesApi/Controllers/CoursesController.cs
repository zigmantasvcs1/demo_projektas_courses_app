﻿using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Models;
using CoursesApi.Services;
using DataAccess.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CoursesApi.Models.Courses.Dtos;

namespace CoursesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        private readonly IService<ListCourseParameter, List<CourseDto>> _listCoursesService;
        private readonly IService<CreateCourseParameter, CourseDto> _createCoursesService;
        private readonly IService<UpdateCourseParameter, CourseDto> _updateCoursesService;
        private readonly IService<GetCourseParameter, CourseDto> _getCoursesService;
        private readonly IService<DeleteCourseParameter, CourseDto> _deleteCoursesService;

        public CoursesController(
            IService<ListCourseParameter, List<CourseDto>> listCoursesService,
            IService<CreateCourseParameter, CourseDto> createCoursesService,
            IService<UpdateCourseParameter, CourseDto> updateCoursesService,
            IService<GetCourseParameter, CourseDto> getCoursesService,
            IService<DeleteCourseParameter, CourseDto> deleteCoursesService)
        {
            _listCoursesService = listCoursesService;
            _createCoursesService = createCoursesService;
            _updateCoursesService = updateCoursesService;
            _getCoursesService = getCoursesService;
            _deleteCoursesService = deleteCoursesService;
        }

        [HttpGet]
        public async Task<IActionResult> ListAsync()
        {
            try
            {
                var courses = await _listCoursesService.CallAsync(
                    new ListCourseParameter(10)
                );

                return Ok(courses);
            }
            catch (Exception ex)
            {
                return StatusCode(
                    500,
                    new Result<Course>(
                        500,
                        null,
                        new List<string>() { "Klaida" }
                    )
                );
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            try
            {
                var result = await _getCoursesService.CallAsync(new GetCourseParameter(id));

                if (result.Data == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<Course>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] CreateCourseDto course)
        {
            var result = await _createCoursesService.CallAsync(
                new CreateCourseParameter(course)
            );

            if (result.Status == 200)
            {
                return Ok(result);
            }

            return StatusCode(
                result.Status,
                result
            );
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, UpdateCourseDto course)
        {
            try
            {
                if (id != course.Id)
                {
                    return BadRequest();
                }

                var result = await _updateCoursesService.CallAsync(
                    new UpdateCourseParameter(course)
                );

                return Ok(result);
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<Course>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                var result = await _deleteCoursesService.CallAsync(new DeleteCourseParameter(id));

                if (result.Data == null)
                {
                    return NotFound();
                }

                return NoContent();
            }
            catch (DbUpdateException)
            {
                return StatusCode(
                    422,
                    new Result<Course>(
                        422,
                        null,
                        new List<string>() { "Kursas yra naudojamas kitur." }
                    )
                );
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<Course>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }
    }
}
