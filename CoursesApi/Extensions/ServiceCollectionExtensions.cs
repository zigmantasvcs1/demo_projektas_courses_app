﻿using CoursesApi.Models.Students.Dtos;
using CoursesApi.Models.Students.Parameters;
using CoursesApi.Services.Students;
using CoursesApi.Services;
using DataAccess;
using CoursesApi.Providers;
using DataAccess.Entities;
using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Models.Lecturers.Dtos;
using CoursesApi.Services.Lecturers;
using CoursesApi.Services.Courses;
using CoursesApi.Models.Courses.Dtos;
using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Models.OngoingCourses.Dtos;
using CoursesApi.Models.OngoingCourses.Parameters;
using CoursesApi.Services.OngoingCourses;

namespace CoursesApi.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            RegisterRepositories(services);

            RegisterServices(services);

            RegisterProviders(services);

            return services;
        }

        private static void RegisterRepositories(IServiceCollection services)
        {
            services.AddTransient<IRepository<Student>, StudentsRepository>();
            services.AddTransient<IRepository<Lecturer>, LecturersRepository>();
            services.AddTransient<IRepository<Course>, CoursesRepository>();
            services.AddTransient<IRepository<OngoingCourse>, OngoingCoursesRepository>();
        }

        private static void RegisterServices(IServiceCollection services)
        {
            #region Student
            services.AddTransient<IService<ListStudentParameter, List<StudentDto>>, ListStudentService>();
            services.Decorate<IService<ListStudentParameter, List<StudentDto>>, ListStudentServiceDecorator>();

            services.AddTransient<IService<CreateStudentParameter, StudentDto>, CreateStudentService>();
            services.Decorate<IService<CreateStudentParameter, StudentDto>, CreateStudentServiceDecorator>();

            services.AddTransient<IService<UpdateStudentParameter, StudentDto>, UpdateStudentService>();
            services.Decorate<IService<UpdateStudentParameter, StudentDto>, UpdateStudentServiceDecorator>();

            services.AddTransient<IService<GetStudentParameter, StudentDto>, GetStudentService>();
            services.Decorate<IService<GetStudentParameter, StudentDto>, GetStudentServiceDecorator>();

            services.AddTransient<IService<DeleteStudentParameter, StudentDto>, DeleteStudentService>();
            services.Decorate<IService<DeleteStudentParameter, StudentDto>, DeleteStudentServiceDecorator>();
            #endregion

            #region Lecturer
            services.AddTransient<IService<ListLecturerParameter, List<LecturerDto>>, ListLecturerService>();
            services.Decorate<IService<ListLecturerParameter, List<LecturerDto>>, ListLecturerServiceDecorator>();

            services.AddTransient<IService<CreateLecturerParameter, LecturerDto>, CreateLecturerService>();
            services.Decorate<IService<CreateLecturerParameter, LecturerDto>, CreateLecturerServiceDecorator>();

            services.AddTransient<IService<UpdateLecturerParameter, LecturerDto>, UpdateLecturerService>();
            services.Decorate<IService<UpdateLecturerParameter, LecturerDto>, UpdateLecturerServiceDecorator>();

            services.AddTransient<IService<GetLecturerParameter, LecturerDto>, GetLecturerService>();
            services.Decorate<IService<GetLecturerParameter, LecturerDto>, GetLecturerServiceDecorator>();

            services.AddTransient<IService<DeleteLecturerParameter, LecturerDto>, DeleteLecturerService>();
            services.Decorate<IService<DeleteLecturerParameter, LecturerDto>, DeleteLecturerServiceDecorator>();
            #endregion

            #region Course
            services.AddTransient<IService<ListCourseParameter, List<CourseDto>>, ListCourseService>();
            services.Decorate<IService<ListCourseParameter, List<CourseDto>>, ListCourseServiceDecorator>();

            services.AddTransient<IService<CreateCourseParameter, CourseDto>, CreateCourseService>();
            services.Decorate<IService<CreateCourseParameter, CourseDto>, CreateCourseServiceDecorator>();

            services.AddTransient<IService<UpdateCourseParameter, CourseDto>, UpdateCourseService>();
            services.Decorate<IService<UpdateCourseParameter, CourseDto>, UpdateCourseServiceDecorator>();

            services.AddTransient<IService<GetCourseParameter, CourseDto>, GetCourseService>();
            services.Decorate<IService<GetCourseParameter, CourseDto>, GetCourseServiceDecorator>();

            services.AddTransient<IService<DeleteCourseParameter, CourseDto>, DeleteCourseService>();
            services.Decorate<IService<DeleteCourseParameter, CourseDto>, DeleteCourseServiceDecorator>();
            #endregion

            #region OngoingCourse
            services.AddTransient<IService<ListOngoingCourseParameter, List<OngoingCourseDto>>, ListOngoingCourseService>();
            services.Decorate<IService<ListOngoingCourseParameter, List<OngoingCourseDto>>, ListOngoingCourseServiceDecorator>();

            services.AddTransient<IService<CreateOngoingCourseParameter, OngoingCourseDto>, CreateOngoingCourseService>();
            services.Decorate<IService<CreateOngoingCourseParameter, OngoingCourseDto>, CreateOngoingCourseServiceDecorator>();

            services.AddTransient<IService<UpdateOngoingCourseParameter, OngoingCourseDto>, UpdateOngoingCourseService>();
            services.Decorate<IService<UpdateOngoingCourseParameter, OngoingCourseDto>, UpdateOngoingCourseServiceDecorator>();

            services.AddTransient<IService<GetOngoingCourseParameter, OngoingCourseDto>, GetOngoingCourseService>();
            services.Decorate<IService<GetOngoingCourseParameter, OngoingCourseDto>, GetOngoingCourseServiceDecorator>();

            services.AddTransient<IService<DeleteOngoingCourseParameter, OngoingCourseDto>, DeleteOngoingCourseService>();
            services.Decorate<IService<DeleteOngoingCourseParameter, OngoingCourseDto>, DeleteOngoingCourseServiceDecorator>();
            #endregion
        }

        private static void RegisterProviders(IServiceCollection services) 
        {
            services.AddTransient<IDateTimeNowProvider, DateTimeNowProvider>();
        }
    }
}
