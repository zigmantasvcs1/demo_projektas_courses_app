﻿using CoursesApi.Models;
using CoursesApi.Models.OngoingCourses.Dtos;
using CoursesApi.Models.OngoingCourses.Parameters;
using CoursesApi.Providers;
using DataAccess;
using DataAccess.Entities;

namespace CoursesApi.Services.OngoingCourses
{
    public class CreateOngoingCourseService : IService<CreateOngoingCourseParameter, OngoingCourseDto>
    {
        private readonly IRepository<OngoingCourse> _ongoingCourseRepository;

        private readonly IDateTimeNowProvider _dateTimeNowProvider;

        public CreateOngoingCourseService(
            IRepository<OngoingCourse> ongoingCourseRepository,
            IDateTimeNowProvider dateTimeNowProvider)
        {
            _ongoingCourseRepository = ongoingCourseRepository;
            _dateTimeNowProvider = dateTimeNowProvider;
        }

        public async Task<Result<OngoingCourseDto>> CallAsync(CreateOngoingCourseParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            // konvertuojame is OngoingCourse dto i entity
            var ongoingCourseEntity = Convert(parameter.OngoingCourse);

            var result = await _ongoingCourseRepository.CreateAsync(ongoingCourseEntity);

            // gauta entity konvertuojame i dto
            var ongoingCourseDto = Convert(result);

            return new Result<OngoingCourseDto>(200, ongoingCourseDto);
        }

        private OngoingCourse Convert(CreateOngoingCourseDto ongoingCourseDto)
        {
            return new OngoingCourse()
            {
                StartDate = ongoingCourseDto.StartDate,
                EndDate = ongoingCourseDto.EndDate,
                CourseId = ongoingCourseDto.CourseId,
                CreatedAt = _dateTimeNowProvider.Get()
            };
        }

        private OngoingCourseDto Convert(OngoingCourse ongoingCourse)
        {
            return new OngoingCourseDto()
            {
                Id = ongoingCourse.Id,
                StartDate = ongoingCourse.StartDate,
                EndDate = ongoingCourse.EndDate
            };
        }
    }
}
