﻿using CoursesApi.Models;
using CoursesApi.Models.Lecturers.Dtos;
using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Services;

namespace CoursesApi.Services.Lecturers
{
    public class GetLecturerServiceDecorator : IService<GetLecturerParameter, LecturerDto>
    {
        private readonly IService<GetLecturerParameter, LecturerDto> _getLecturerService;
        private readonly ILogger<GetLecturerServiceDecorator> _logger;

        public GetLecturerServiceDecorator(
            IService<GetLecturerParameter, LecturerDto> getLecturerService,
            ILogger<GetLecturerServiceDecorator> logger)
        {
            _getLecturerService = getLecturerService;
            _logger = logger;
        }

        public async Task<Result<LecturerDto>> CallAsync(GetLecturerParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _getLecturerService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<LecturerDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
