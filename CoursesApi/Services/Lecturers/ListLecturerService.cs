﻿using CoursesApi.Models;
using CoursesApi.Models.Lecturers.Dtos;
using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Services;
using DataAccess;
using DataAccess.Entities;

namespace CoursesApi.Services.Lecturers
{
    public class ListLecturerService : IService<ListLecturerParameter, List<LecturerDto>>
    {
        private readonly IRepository<Lecturer> _lecturersRepository;

        public ListLecturerService(IRepository<Lecturer> lecturersRepository)
        {
            _lecturersRepository = lecturersRepository;
        }

        public async Task<Result<List<LecturerDto>>> CallAsync(ListLecturerParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _lecturersRepository.ListAsync(parameter.Limit);

            var lecturerDtos = result
                .Select(Convert)
                .ToList();

            return new Result<List<LecturerDto>>(200, lecturerDtos);
        }

        private LecturerDto Convert(Lecturer lecturer)
        {
            return new LecturerDto()
            {
                Id = lecturer.Id,
                Name = lecturer.Name,
                Surname = lecturer.Surname,
                Email = lecturer.Email,
                DocumentNumber = lecturer.DocumentNumber
            };
        }

        private int? ResolveAge(DateTime? birthDay)
        {
            if (birthDay.HasValue)
            {
                var today = DateTime.Today;

                var age = today.Year - birthDay.Value.Year;

                if (birthDay.Value.Date > today.AddYears(-age))
                {
                    age--;
                }

                return age;
            }
            return null;
        }
    }
}
