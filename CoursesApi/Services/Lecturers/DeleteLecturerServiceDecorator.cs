﻿using CoursesApi.Models;
using CoursesApi.Models.Lecturers.Dtos;
using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Services;

namespace CoursesApi.Services.Lecturers
{
    public class DeleteLecturerServiceDecorator : IService<DeleteLecturerParameter, LecturerDto>
    {
        private readonly IService<DeleteLecturerParameter, LecturerDto> _deleteLecturerService;
        private readonly ILogger<DeleteLecturerServiceDecorator> _logger;

        public DeleteLecturerServiceDecorator(
            IService<DeleteLecturerParameter, LecturerDto> deleteLecturerService,
            ILogger<DeleteLecturerServiceDecorator> logger)
        {
            _deleteLecturerService = deleteLecturerService;
            _logger = logger;
        }

        public async Task<Result<LecturerDto>> CallAsync(DeleteLecturerParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _deleteLecturerService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<LecturerDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
