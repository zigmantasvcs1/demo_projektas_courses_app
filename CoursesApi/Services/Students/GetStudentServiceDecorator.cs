﻿using CoursesApi.Models;
using CoursesApi.Models.Students.Dtos;
using CoursesApi.Models.Students.Parameters;

namespace CoursesApi.Services.Students
{
    public class GetStudentServiceDecorator : IService<GetStudentParameter, StudentDto>
    {
        private readonly IService<GetStudentParameter, StudentDto> _getStudentService;
        private readonly ILogger<GetStudentServiceDecorator> _logger;

        public GetStudentServiceDecorator(
            IService<GetStudentParameter, StudentDto> getStudentService,
            ILogger<GetStudentServiceDecorator> logger)
        {
            _getStudentService = getStudentService;
            _logger = logger;
        }

        public async Task<Result<StudentDto>> CallAsync(GetStudentParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _getStudentService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<StudentDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
