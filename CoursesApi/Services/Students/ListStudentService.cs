﻿using CoursesApi.Models;
using CoursesApi.Models.Students.Dtos;
using CoursesApi.Models.Students.Parameters;
using DataAccess;
using DataAccess.Entities;

namespace CoursesApi.Services.Students
{
    public class ListStudentService : IService<ListStudentParameter, List<StudentDto>>
    {
        private readonly IRepository<Student> _studentsRepository;

        public ListStudentService(IRepository<Student> studentsRepository)
        {
            _studentsRepository = studentsRepository;
        }

        public async Task<Result<List<StudentDto>>> CallAsync(ListStudentParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _studentsRepository.ListAsync(parameter.Limit);

            var studentDtos = result
                .Select(Convert)
                .ToList();

            return new Result<List<StudentDto>>(200, studentDtos);
        }

        private StudentDto Convert(Student student)
        {
            return new StudentDto()
            {
                Id = student.Id,
                Name = student.Name,
                Surname = student.Surname,
                Email = student.Email,
                BirthDay = student.BirthDay,
                DocumentNumber = student.DocumentNumber,
                Age = ResolveAge(student.BirthDay)
            };
        }

        private int? ResolveAge(DateTime? birthDay)
        {
            if (birthDay.HasValue)
            {
                var today = DateTime.Today;

                var age = today.Year - birthDay.Value.Year;

                if (birthDay.Value.Date > today.AddYears(-age))
                {
                    age--;
                }

                return age;
            }
            return null;
        }
    }
}
