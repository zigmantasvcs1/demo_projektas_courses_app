﻿using CoursesApi.Models;
using CoursesApi.Models.Students.Dtos;
using CoursesApi.Models.Students.Parameters;

namespace CoursesApi.Services.Students
{
    public class CreateStudentServiceDecorator : IService<CreateStudentParameter, StudentDto>
    {
        private readonly IService<CreateStudentParameter, StudentDto> _createStudentService;
        private readonly ILogger<CreateStudentServiceDecorator> _logger;

        public CreateStudentServiceDecorator(
            IService<CreateStudentParameter, StudentDto> createStudentService,
            ILogger<CreateStudentServiceDecorator> logger)
        {
            _createStudentService = createStudentService;
            _logger = logger;
        }

        public async Task<Result<StudentDto>> CallAsync(CreateStudentParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _createStudentService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<StudentDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
