﻿using CoursesApi.Models;
using CoursesApi.Models.Courses.Dtos;
using CoursesApi.Models.Courses.Parameters;
using DataAccess;
using DataAccess.Entities;

namespace CoursesApi.Services.Courses
{
    public class GetCourseService : IService<GetCourseParameter, CourseDto>
    {
        private readonly IRepository<Course> _courseRepository;

        public GetCourseService(IRepository<Course> courseRepository)
        {
            _courseRepository = courseRepository;
        }

        public async Task<Result<CourseDto>> CallAsync(GetCourseParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _courseRepository.GetAsync(parameter.Id);

            var courseDto = Convert(result);

            return new Result<CourseDto>(200, courseDto);
        }

        private CourseDto Convert(Course course)
        {
            return new CourseDto()
            {
                Id = course.Id,
                Price = course.Price,
                Title = course.Title,
                Hours = course.Hours,
                Description = course.Description
            };
        }
    }
}
