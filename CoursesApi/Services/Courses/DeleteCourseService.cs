﻿using CoursesApi.Models;
using CoursesApi.Models.Courses.Dtos;
using CoursesApi.Models.Courses.Parameters;
using DataAccess;
using DataAccess.Entities;

namespace CoursesApi.Services.Courses
{
    public class DeleteCourseService : IService<DeleteCourseParameter, CourseDto>
    {
        private readonly IRepository<Course> _courseRepository;

        public DeleteCourseService(IRepository<Course> courseRepository)
        {
            _courseRepository = courseRepository;
        }

        public async Task<Result<CourseDto>> CallAsync(DeleteCourseParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _courseRepository.DeleteAsync(parameter.Id);

            if (result)
            {
                return new Result<CourseDto>(200, new CourseDto());
            }

            return new Result<CourseDto>(404, null);
        }
    }
}
