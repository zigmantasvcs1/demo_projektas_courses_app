﻿using CoursesApi.Models;
using CoursesApi.Models.Courses.Dtos;
using CoursesApi.Models.Courses.Parameters;
using DataAccess;
using DataAccess.Entities;

namespace CoursesApi.Services.Courses
{
    public class UpdateCourseService : IService<UpdateCourseParameter, CourseDto>
    {
        private readonly IRepository<Course> _courseRepository;

        public UpdateCourseService(IRepository<Course> courseRepository)
        {
            _courseRepository = courseRepository;
        }

        public async Task<Result<CourseDto>> CallAsync(UpdateCourseParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var courseToUpdate = await _courseRepository.GetAsync(parameter.Course.Id);

            var courseEntity = Convert(parameter.Course, courseToUpdate);

            var result = await _courseRepository.UpdateAsync(courseEntity);

            var courseDto = Convert(result);

            return new Result<CourseDto>(200, courseDto);
        }

        private Course Convert(UpdateCourseDto courseDto, Course courseEntity)
        {
            courseEntity.Price = courseDto.Price;
            courseEntity.Title = courseDto.Title;
            courseEntity.Hours = courseDto.Hours;
            courseEntity.Description = courseDto.Description;

            return courseEntity;
        }

        private CourseDto Convert(Course course)
        {
            return new CourseDto()
            {
                Id = course.Id,
                Price = course.Price,
                Title = course.Title,
                Hours = course.Hours,
                Description = course.Description
            };
        }
    }
}
