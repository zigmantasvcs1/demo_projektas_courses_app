﻿using CoursesApi.Models;
using CoursesApi.Models.Courses.Dtos;
using CoursesApi.Models.Courses.Parameters;

namespace CoursesApi.Services.Courses
{
    public class GetCourseServiceDecorator : IService<GetCourseParameter, CourseDto>
    {
        private readonly IService<GetCourseParameter, CourseDto> _getCourseService;
        private readonly ILogger<GetCourseServiceDecorator> _logger;

        public GetCourseServiceDecorator(
            IService<GetCourseParameter, CourseDto> getCourseService,
            ILogger<GetCourseServiceDecorator> logger)
        {
            _getCourseService = getCourseService;
            _logger = logger;
        }

        public async Task<Result<CourseDto>> CallAsync(GetCourseParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _getCourseService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<CourseDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
