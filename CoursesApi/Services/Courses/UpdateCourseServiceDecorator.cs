﻿using CoursesApi.Models;
using CoursesApi.Models.Courses.Dtos;
using CoursesApi.Models.Courses.Parameters;

namespace CoursesApi.Services.Courses
{
    public class UpdateCourseServiceDecorator : IService<UpdateCourseParameter, CourseDto>
    {
        private readonly IService<UpdateCourseParameter, CourseDto> _updateCourseService;
        private readonly ILogger<UpdateCourseServiceDecorator> _logger;

        public UpdateCourseServiceDecorator(
            IService<UpdateCourseParameter, CourseDto> updateCourseService,
            ILogger<UpdateCourseServiceDecorator> logger)
        {
            _updateCourseService = updateCourseService;
            _logger = logger;
        }

        public async Task<Result<CourseDto>> CallAsync(UpdateCourseParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _updateCourseService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<CourseDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
