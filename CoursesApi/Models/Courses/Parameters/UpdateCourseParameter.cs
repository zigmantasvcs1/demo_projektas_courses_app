﻿using CoursesApi.Models.Courses.Dtos;

namespace CoursesApi.Models.Courses.Parameters
{
    public class UpdateCourseParameter
    {
        public UpdateCourseParameter(UpdateCourseDto course)
        {
            Course = course;
        }

        public UpdateCourseDto Course { get; }
    }
}
