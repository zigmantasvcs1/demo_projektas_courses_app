﻿namespace CoursesApi.Models.Courses.Parameters
{
    public class DeleteCourseParameter
    {
        public DeleteCourseParameter(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
