﻿namespace CoursesApi.Models.Courses.Dtos
{
    public class CourseDto
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public string Title { get; set; }
        public int Hours { get; set; }
        public string Description { get; set; }
    }
}
