﻿using CoursesApi.Models.Lecturers.Dtos;

namespace CoursesApi.Models.Lecturers.Parameters
{
    public class CreateLecturerParameter
    {
        public CreateLecturerParameter(CreateLecturerDto lecturer)
        {
            Lecturer = lecturer;
        }

        public CreateLecturerDto Lecturer { get; }
    }
}
