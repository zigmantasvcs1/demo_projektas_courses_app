﻿using CoursesApi.Models.Students.Dtos;

namespace CoursesApi.Models.Students.Parameters
{
    public class CreateStudentParameter
    {
        public CreateStudentParameter(CreateStudentDto student)
        {
            Student = student;
        }

        public CreateStudentDto Student { get; }
    }
}
