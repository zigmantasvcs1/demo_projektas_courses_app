﻿using CoursesApi.Models.Students.Dtos;
using DataAccess.Entities;

namespace CoursesApi.Models.Students.Parameters
{
    public class UpdateStudentParameter
    {
        public UpdateStudentParameter(UpdateStudentDto student)
        {
            Student = student;
        }

        public UpdateStudentDto Student { get; }
    }
}
