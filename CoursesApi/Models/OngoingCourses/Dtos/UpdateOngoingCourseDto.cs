﻿namespace CoursesApi.Models.OngoingCourses.Dtos
{
    public class UpdateOngoingCourseDto
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
