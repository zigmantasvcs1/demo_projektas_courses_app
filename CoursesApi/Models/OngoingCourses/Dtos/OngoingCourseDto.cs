﻿using CoursesApi.Models.Courses.Dtos;

namespace CoursesApi.Models.OngoingCourses.Dtos
{
    public class OngoingCourseDto
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public CourseDto? Course { get; set; }
    }
}
