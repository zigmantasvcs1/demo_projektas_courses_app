﻿using CoursesApi.Models.OngoingCourses.Dtos;

namespace CoursesApi.Models.OngoingCourses.Parameters
{
    public class UpdateOngoingCourseParameter
    {
        public UpdateOngoingCourseParameter(UpdateOngoingCourseDto ongoingCourse)
        {
            OngoingCourse = ongoingCourse;
        }

        public UpdateOngoingCourseDto OngoingCourse { get; }
    }
}
