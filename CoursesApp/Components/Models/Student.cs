﻿namespace CoursesApp.Components.Models
{
    public class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime? BirthDay { get; set; }
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
