﻿namespace CoursesApp.Components.Models
{
    public class Response
    {
        public int Status { get; set; }
        public List<Student> Data { get; set; }
        public List<string> Errors { get; set; }
    }
}
